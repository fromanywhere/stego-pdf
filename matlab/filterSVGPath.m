function [processedPathStruct, protectedPagesStat] = filterSVGPath(sourcePathStruct, countThreshold, lengthThreshold, protectedPages)
    [processedPathStruct, rankStruct] = rankSVGPath(sourcePathStruct, countThreshold, lengthThreshold);
    
    if ~exist('protectedPages','var')
        protectedPages = 1:sourcePathStruct.pageCount;
    end    
    
    goodSymbolsCount = numel(rankStruct(:, 1));
    benefitSymbolsCount = 0;
  
    protectCoefficient = 1/countThreshold;
    
    protectedPagesCount = numel(protectedPages);
    protectedPagesStat = cell(protectedPagesCount+2, 0);   
    protectedPagesIndex = cell(protectedPagesCount, 1);
    
    for i = 1:protectedPagesCount
        protectedPagesIndex{i} = strcat('page_', num2str(protectedPages(i)));
    end
   
    for i = 1:goodSymbolsCount
        
        % �������� �� ���������� ���������� ��� ����������� ��������
        if ~logical(rankStruct{i,2})
            continue;
        end
        
        currentSymbolUsingPerPage = cell(protectedPagesCount+2, 0);
        currentSymbolUsingPerPage{1} = rankStruct{i,1};
        currentSymbolUsingPerPage{2} = rankStruct{i,2};
        currentSymbolBenefit = 0;
        
        for j = 1:protectedPagesCount
            howManyCurrentSymbolUsagesPerPageCanBeReplaced = 0;
            currentSymbolPerPageUsing = fieldnames(processedPathStruct.symbols.(rankStruct{i}).pageUsage);
            
            if logical(numel(find(strcmp(protectedPagesIndex{j}, currentSymbolPerPageUsing))))
                howManyCurrentSymbolUsagesPerPageCanBeReplaced = floor(processedPathStruct.symbols.(rankStruct{i}).pageUsage.(protectedPagesIndex{j}) * protectCoefficient);    
            end
            
           currentSymbolUsingPerPage{j+2} = howManyCurrentSymbolUsagesPerPageCanBeReplaced; 
           currentSymbolBenefit = currentSymbolBenefit + howManyCurrentSymbolUsagesPerPageCanBeReplaced; 
        end
        
        if logical(currentSymbolBenefit)
            benefitSymbolsCount = benefitSymbolsCount+1;
            protectedPagesStat = [protectedPagesStat; currentSymbolUsingPerPage];
        end        
    end
    
    benefitSymbolsCount    
end