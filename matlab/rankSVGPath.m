% ��������� ������ ���� � �������� ��������� �� ���� ���������
% ���������� ����������� ���������, ���������� ������, � ������� ����� ��
% ���������
%
% �������������:
% [<�������������� ���������>, <�������>] = rankSVGPath(<�������� ���������>, <����� �� ���������� ��������� ������� � ��������>, <����� �� ����������� ����� ������� �������>)
%
% ������� ������������ �����  cell array ��������� ���������:
% {<��������� ������ �������, ��������, svg_1_1>, <���������� �������� ��������, ����������� �������� ����� �� �����>, <��������� ����� �������>}
%
% ������� ������ ����� ��������� ������� ����� ��������������� �������� �
% ��������������, ��� ������� �� [<��������� ���������� ����� ��������� � �������� ������� ����>, <����� ���������� ���������� ����� 1-2, 2-3, 3-4 ������ ������ �����>]
%
% ������� ������ ������������ � ��������� � �����������, ��������
%
% sortrows(rankRating, -2) ����������� ������� �� ����������
% ������������������ �������� ��������
%
% sortrows(rankRating, -3) ����������� �� ��������� ����� �������

function [updatePathStruct, rankRating] = rankSVGPath(pathStruct, countThreshold, lengthThreshold)
    updatePathStruct = pathStruct;
    rankRating = cell(0, 3);
   
    if ~exist('lengthThreshold','var')
        lengthThreshold = 0;
    end       
    
    if ~exist('countThreshold','var')
        countThreshold = 0;
    end
        
    pathFields = fieldnames(pathStruct.symbols);
    
    for currentPath = 1:numel(pathFields)
        if (pathStruct.symbols.(pathFields{currentPath}).count > countThreshold)
            updatePathStruct.symbols.(pathFields{currentPath}).bezier = parseSVGPath(pathStruct.symbols.(pathFields{currentPath}).path);
            pathLength = getPathLength(updatePathStruct.symbols.(pathFields{currentPath}).bezier);

            updatePathStruct.symbols.(pathFields{currentPath}).pathLength = pathLength;
            %updatePathStruct.symbols.(pathFields{currentPath}).longPathsCount = getLongPathsCount(pathLength, lengthThreshold); 

            [resultRankPoint, rankPoint, indexPoint, linkPoint] = voteSVGPath(updatePathStruct.symbols.(pathFields{currentPath}), [10, 0.1, lengthThreshold]);
            
            updatePathStruct.symbols.(pathFields{currentPath}).resultRankPoint = resultRankPoint;
            updatePathStruct.symbols.(pathFields{currentPath}).rankPoint = rankPoint;
            updatePathStruct.symbols.(pathFields{currentPath}).indexPoint = indexPoint;
            updatePathStruct.symbols.(pathFields{currentPath}).linkPoint = linkPoint;
            
            updatePathStruct.symbols.(pathFields{currentPath}).embeddablePoints = getEmbeddablePointsCount(resultRankPoint);
            
            rankRating = [rankRating; {pathFields{currentPath}, updatePathStruct.symbols.(pathFields{currentPath}).embeddablePoints, sum(pathLength);}];
        end
    end
end

function result = getPathLength(bezierPaths)
    result = mean([getShortDistance(bezierPaths), getLongDistance(bezierPaths)], 2);
end

function result = getLongPathsCount(bezierPaths, threshold)
    result = numel(find(bezierPaths > threshold));
end

function result = getShortDistance(bezierPaths)
    diffX = bezierPaths(:, 1) - bezierPaths(:, 7);
    diffY = bezierPaths(:, 2) - bezierPaths(:, 8);
    
    result = sqrt(diffX.^2 + diffY.^2);
end

function result = getLongDistance(bezierPaths)
    diffX1 = bezierPaths(:, 1) - bezierPaths(:, 3);
    diffY1 = bezierPaths(:, 2) - bezierPaths(:, 4);
    diffX2 = bezierPaths(:, 3) - bezierPaths(:, 5);
    diffY2 = bezierPaths(:, 4) - bezierPaths(:, 6);
    diffX3 = bezierPaths(:, 5) - bezierPaths(:, 7);
    diffY3 = bezierPaths(:, 6) - bezierPaths(:, 8);
    
    result = sum([sqrt(diffX1.^2 + diffY1.^2), sqrt(diffX2.^2 + diffY2.^2), sqrt(diffX3.^2 + diffY3.^2)], 2);
end
 
function result = getEmbeddablePointsCount(resultRankPoint)
    result = numel(find(resultRankPoint < Inf));
end