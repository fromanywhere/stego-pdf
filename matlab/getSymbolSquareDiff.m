function result = getSymbolSquareDiff(originalPalette, newPalette)
    result = 0;
    count = numel(originalPalette);
    
    % ���� ������� �� ���������, ���� �� �������� �������, �� �����
    if count == numel(newPalette)
        
        % ���� ���� ������� 0, � ������ �� 0, ������ ������� ����� ��
        % �������� �� ����� ������������� ������� ����� ����������
        % ����� ������ ����������������, �� ��������� ��������
        for i = 1:count
            if (originalPalette(i) == 0 && newPalette(i) ~= 0) || (originalPalette(i) ~= 0 && newPalette(i) == 0)
                newPalette(i) = originalPalette(i); 
            end
        end
        
        result = sum(abs(originalPalette - newPalette));
    end
end