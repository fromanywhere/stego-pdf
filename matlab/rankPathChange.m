function result = rankPathChange(symbol, candidatePointIndex)
    stepThetaValue = 0.10; % � ������������ � voteSVGPath.getBezierSupplyRank
    stepRhoValue = 0.15; % �� ����� ������ �������, ����� ��������� ����� �����
    stepThetaCount = 3;
    stepRhoCount = 5; % � ��� ���� ������ �� ����������� ���������, ������� �������� �������� 
    result = zeros(stepThetaCount*stepRhoCount, 3);
    
    linkedCandidatePointIndex = symbol.linkPoint(candidatePointIndex);
    
    if symbol.linkPoint(linkedCandidatePointIndex, 3) ~= 0 % ��� ���������� ������ ������� �����
        return;    
    end
    
    % ������� ���������� �����-��������� � ��������� � ��� ������� �����
    currentCandidatePointCoords = symbol.indexPoint(candidatePointIndex, :);
    linkedCandidatePointCoords = symbol.indexPoint(linkedCandidatePointIndex(1), :);
    
    % �������� � �������� ������� ���������
    normalizeCoords = currentCandidatePointCoords - linkedCandidatePointCoords;
    [theta, rho] = cart2pol(normalizeCoords(1), normalizeCoords(2));
    
    % ���������� ������� � ��� ���������
    stepTheta = theta*stepThetaValue; 
    startTheta = theta - stepTheta*((stepThetaCount-1)/2);
    endTheta = theta + stepTheta*((stepThetaCount-1)/2);
    
    stepRho = rho*stepRhoValue; 
    startRho = rho - stepRho*((stepRhoCount-1)/2);
    endRho = rho + stepRho*((stepRhoCount-1)/2); 
    
    % ����� ������
    [~, originalPalette] = getSymbolSquare(symbol.bezier);
    
    % ��� ����� �����������
    for currentTheta = startTheta:stepTheta:endTheta
                
        for currentRho = startRho:stepRho:endRho
            % �������� ����� ���������� �� ������ ������
            bezierModification = pathChange(symbol, candidatePointIndex, [currentTheta, currentRho]);
            if not(checkForCorrectModification(symbol.bezier, bezierModification))
                continue;
            end
            
            % ��������� �������
            [canvas, newPalette] = getSymbolSquare(bezierModification);         
            
            % ����� ������� � ��������
            squareDiff = getSymbolSquareDiff(originalPalette, newPalette);
            
            squareDiff = squareDiff / numel(canvas);
            
            % ���� ���� ���� �����-�� ���������, ���������
            if (squareDiff > result(3))
                result = [currentTheta, currentRho, squareDiff];
            end           
        end
    end 
end

function result = checkForCorrectModification(originalPath, changedPath)
    result = true;
    
    % ���������� �� voteSVGPath.getBezierSupplyRank
    bezierAngleThreshold = 10;
    bezierMinHandleLengthCoefficient = 0.1;
    bezierMaxHandleLengthCoefficient = 0.5;    
    
    % �� ����������� ������� �������� ���������� ������
    diff = originalPath - changedPath;
    [yIndex, ~] = find(diff); % ������ ���� ������, � ���������� ����� ���
    
    % ��� �����������
    % ���� ��������� ���, ������ ������������� ���������
    % ��� ���� ����������� �� ������ ����� ��������� �������� ��������� �������
    if numel(yIndex) == 0
        result = false;
        return;
    end
    
    path = changedPath(yIndex(1), :);
    
    % �� ������� �������� ���������� �����
    pointA = path(1:2);
    pointB = path(3:4);
    pointC = path(5:6);
    pointD = path(7:8);
        
    % ���� ��� ���� ������� ������������ ���� �� ������, ������
    d1 = pointB - pointA;
    a1 = 90;
    if d1(1) ~= 0
        a1 = atan(d1(2)/d1(1)) * 180 / pi;
    else
        a1 = d1(2)/abs(d1(2)) * a1;
    end    

    d2 = pointC - pointD;
    a2 = 90;
    if d2(1) ~= 0
        a2 = atan(d2(2)/d2(1)) * 180 / pi;
    else
        a2 = d2(2)/abs(d2(2)) * a2;
    end 

    if ((abs(mod(a1, 90)) + abs(mod(a2, 90))) < bezierAngleThreshold) || (abs(a2 - a1) < bezierAngleThreshold)               
        result = false;
        return;
    end

    % ���� ����� ������ ������ ��� ������ �������� ���������
    directPathLength = sqrt(sum((pointA - pointD).^2));
    APathLength = sqrt(sum((pointA - pointB).^2)); 
    BPathLength = sqrt(sum((pointC - pointD).^2)); 

    if APathLength > bezierMaxHandleLengthCoefficient * directPathLength                
        result = false;
        return;
    end
    if APathLength < bezierMinHandleLengthCoefficient * directPathLength                
        result = false;
        return;
    end            

    if BPathLength > bezierMaxHandleLengthCoefficient * directPathLength                
        result = false;
        return;
    end    
    if BPathLength < bezierMinHandleLengthCoefficient * directPathLength                
        result = false;
        return;
    end

    % ���� ���������� ����� ������������ ������� ������� ����
    handleDistance = sqrt(sum((pointB - pointC).^2));
    if handleDistance < bezierMinHandleLengthCoefficient * directPathLength
        result = false;
        return;
    end
end