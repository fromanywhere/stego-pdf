function result = pathChange(symbol, candidatePointIndexArray, changeParams)
    result = symbol.bezier;
    
    % �������� �� �������� ������ � ���� ������� ��������� �����
    % ���������, �������� �������� ����� � ����������� [Ro, Theta]
    for currentCandidatePoint = 1:numel(candidatePointIndexArray)
        candidatePointIndex = candidatePointIndexArray(currentCandidatePoint); 
        
        linkedCandidatePointIndex = symbol.linkPoint(candidatePointIndex);

        if symbol.linkPoint(linkedCandidatePointIndex, 3) ~= 0 % ��� ���������� ������ ������� �����
            continue;    
        end

        % ������� ���������� �����-��������� � ��������� � ��� ������� �����
        currentCandidatePointCoords = symbol.indexPoint(candidatePointIndex, :);
        linkedCandidatePointCoords = symbol.indexPoint(linkedCandidatePointIndex(1), :);

        [polarX, polarY] = pol2cart(changeParams(currentCandidatePoint, 1), changeParams(currentCandidatePoint, 2));
        newCoords = linkedCandidatePointCoords + [polarX, polarY];

        % �������� ����� ���������� �� ������ ������ � ���������
        [yIndex, xIndex] = findsubmat(result, currentCandidatePointCoords);
        result(yIndex, xIndex:xIndex+1) = newCoords;               
    end
   
end