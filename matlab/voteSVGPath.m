function [resultRankPoint, rankPoint, indexPoint, linkPoint] = voteSVGPath(letter, rankThreshold, rankWeight)

    if ~exist('rankThreshold','var')
        rankThreshold = [5, 0.1, 0];
    end  

    if ~exist('rankWeight','var')
        rankWeight = [1, 1, 1, 3, 3, 2, 2, 10, 10, 1];
    end    
    
    indexPoint = getPointIndex(letter); % ������ ���������� ����� ����
    lengthPointWeight = getPointWeight(letter, indexPoint); % ������ ����� �������� � ������������ ����� ��������, �� ������� ��� �����
    linkPoint = getPointLinks(letter, indexPoint); % �������� ���������� � ������ ����� ��������� ������� � ����� �����
    bezierSupplyRank = getBezierSupplyRank(letter, indexPoint, rankWeight); % �������� ���������� � ������� ��� ����������� ����� ������ �����
    rankPoint = getPointRank(indexPoint, lengthPointWeight, linkPoint, bezierSupplyRank, rankThreshold); % ������� ������� ��� ������ �����
    
    % �������� ������ �������� ���������� ������� � ��������� ������� �����
    resultRankPoint = zeros(numel(rankPoint)/10, 1);
    for i = 1:numel(resultRankPoint)
        resultRankPoint(i) = sum(rankPoint(i, :) .* rankWeight);
        
        % �������������� ���� ����� ������
        % ���� ������� ������� �����������+�������������, ���������
        if (rankPoint(i, 2) * rankPoint(i, 3)) ~= 0
            resultRankPoint(i) = Inf;
        end
        
        % ���� ������� ������� ������������ � �����������, 
        % ��������� ����������� ������� �����
        if (rankPoint(i, 4) * rankPoint(i, 5)) ~= 0
            resultRankPoint(i) = Inf;
        end  
        
        % ����������� ������� �������� � ���� ������
        % ���� ���������
        if (rankPoint(i, 6) * rankPoint(i, 7)) ~= 0
            resultRankPoint(i) = Inf;
        end     
        
        % ����������� ����� �������� �������� ���������
        if rankPoint(i, 8) ~= 0
            resultRankPoint(i) = Inf;
        end   
        
        % ����������� ����������� ����� ���������
        if rankPoint(i, 9) ~= 0
            resultRankPoint(i) = Inf;
        end  
        
        % ����������� �������� �����������
        if (rankPoint(i, 4) ~= 0) && (rankPoint(i, 2) == 2 || rankPoint(i, 3) == 2)
            resultRankPoint(i) = Inf;
        end
    end

    % drawSVGPath(letter.bezier, {indexPoint, num2str(rankPoint)});
end

% ���������� ������ ��������� ���������� �����
% ������ ��������� �������� �������� ��������������� ���������� �����
function result = getPointIndex(path)
    result = zeros(0, 2);
    
    % ��� �������� ���� ����� � �������� ����
    for i = 1:numel(path.pathLength)
        for j = 1:2:8
            testPoint = path.bezier(i, j:j+1);
            isPointIndexExists = false;
            
            % ���������, ���� �� ������� ����� � �������
            for k = 1:numel(result)/2
                %if isequal(result(k, 1:2), testPoint);
                % ������ 
                if (result(k, 1) == testPoint(1)) && (result(k, 2) == testPoint(2))
                    isPointIndexExists = true;
                    break;
                end
            end
            
            % ���� ���, ���������
            if ~isPointIndexExists
                result = [result; testPoint];
            end
        end
    end
end

% ���������� ������ ���� ��������, ��������������� ������� �������
% ���������� �����
function result = getPointWeight(path, indexPoint)
    result = zeros(numel(indexPoint)/2, 2);
    
    % ��� �������� ���� �������� ����� ������������ ���������� � �����
    % �������. �������� � �����, �������� � �������
    for i = 1:numel(path.bezier)/8
        for j = 1:2:8
            index = getIndexByValue(indexPoint, path.bezier(i, j:j+1));            
            result(index, :) = setSiblingPoint(result(index, :), path.pathLength(i));
        end
    end
end

% ���������� ������ ������ ����� �������
% � �����, ��������������� ��������, ����� ���� �� 1 �� 2 �������� �����,
% ��������������� �� ���������. ������ ���������� ����������� � ���� ������
% �����
function result = getPointLinks(path, indexPoint)
    result = zeros(numel(indexPoint)/2, 3);
    result(:, 3) = -1;
    
    % ���� �����, ����������� � ������� �������
    % 0 ������� �����
    % 1 ����� ������ ������
    % 2 ����������� ����� ������ ������

    for i = 1:numel(path.pathLength)
        pointA = path.bezier(i, 1:2);
        pointB = path.bezier(i, 3:4);
        pointC = path.bezier(i, 5:6);
        pointD = path.bezier(i, 7:8);
        
        Aindex = getIndexByValue(indexPoint, pointA);
        Dindex = getIndexByValue(indexPoint, pointD);
        
        result(Aindex, :) = [result(Aindex, 1:2), 0];
        result(Dindex, :) = [result(Dindex, 1:2), 0];
        
        % ����, ����� ����� ������, ���������� ��������
        if isequal(pointB, pointC)
            BCindex = getIndexByValue(indexPoint, pointB);

            % Z-������������ �������, ������������ ������, ����� ������������, ��������, 0 �� 2
            if (result(BCindex, 3) == -1)
                result(BCindex, :) = [result(BCindex, 1:2), 2];

                result(BCindex, :) = setSiblingPoint(result(BCindex, :), Aindex);
                result(BCindex, :) = setSiblingPoint(result(BCindex, :), Dindex);
            end
            
            result(Aindex, :) = setSiblingPoint(result(Aindex, :), BCindex);
            result(Dindex, :) = setSiblingPoint(result(Dindex, :), BCindex);
        else
            Bindex = getIndexByValue(indexPoint, pointB);
            Cindex = getIndexByValue(indexPoint, pointC);
            
            result(Bindex, :) = [result(Bindex, 1:2), 1];
            result(Cindex, :) = [result(Cindex, 1:2), 1];            
            
            result(Aindex, :) = setSiblingPoint(result(Aindex, :), Bindex);
            result(Bindex, :) = setSiblingPoint(result(Bindex, :), Aindex);
            result(Cindex, :) = setSiblingPoint(result(Cindex, :), Dindex);
            result(Dindex, :) = setSiblingPoint(result(Dindex, :), Cindex);
        end
    end
end

% ���������� ������ ���������� ����� �� �������� � ���������
function result = getIndexByValue(indexPoint, point)
    result = 0;

    for k = 1:numel(indexPoint)/2
        %if isequal(indexPoint(k, 1:2), point);
        % ������ �������� ������� �������
        if (indexPoint(k, 1) == point(1)) && (indexPoint(k, 2) == point(2))
            result = k;
            break;
        end
    end    
end

% ������������� ��������, �������� ��������� ����
function result = setSiblingPoint(pointVector, value)
    result = pointVector;

    if result(1) == 0
        result(1) = value;
    elseif result(2) == 0
        result(2) = value;
    end    
end

% ����������� ����� ����������� ������ ��� ������ �����
% ���� ���� ����� �������� ������� ������
function result = getBezierSupplyRank(path, indexPoint, rankWeight)
    result = zeros(numel(indexPoint)/2);
           
    bezierAngleThreshold = 10;
    bezierMinHandleLengthCoefficient = 0.1;
    bezierMaxHandleLengthCoefficient = 0.5;
    
    % ������� ������ ��� ������
    for i = 1:numel(path.bezier)/8     
        if ~isequal(path.bezier(i, 3:4), path.bezier(i, 5:6))
            pointA = path.bezier(i, 1:2);
            pointB = path.bezier(i, 3:4);
            pointC = path.bezier(i, 5:6);
            pointD = path.bezier(i, 7:8);
            
            Bindex = getIndexByValue(indexPoint, pointB);
            Cindex = getIndexByValue(indexPoint, pointC);            
  
            % ���� ��� ���� ������� ������������ ���� �� ������, ������
            % �����
            d1 = pointB - pointA;
            a1 = 90;
            if d1(1) ~= 0
                a1 = atan(d1(2)/d1(1)) * 180 / pi;
            else
                a1 = d1(2)/abs(d1(2)) * a1;
            end    
            
            d2 = pointC - pointD;
            a2 = 90;
            if d2(1) ~= 0
                a2 = atan(d2(2)/d2(1)) * 180 / pi;
            else
                a2 = d2(2)/abs(d2(2)) * a2;
            end 
            
            if ((abs(mod(a1, 90)) + abs(mod(a2, 90))) < bezierAngleThreshold) || (abs(a2 - a1) < bezierAngleThreshold)               
                result(Bindex) = result(Bindex) + rankWeight(10);
                result(Cindex) = result(Cindex) + rankWeight(10);
            end
            
            % ���� ����� ������ ������ ��� ������ �������� ���������,
            % ������ �����
            directPathLength = sqrt(sum((pointA - pointD).^2));
            APathLength = sqrt(sum((pointA - pointB).^2)); 
            BPathLength = sqrt(sum((pointC - pointD).^2)); 
            
            if APathLength > bezierMaxHandleLengthCoefficient * directPathLength                
                result(Bindex) = result(Bindex) + rankWeight(10);
            end
            if APathLength < bezierMinHandleLengthCoefficient * directPathLength                
                result(Bindex) = result(Bindex) + rankWeight(10);
            end            
            
            if BPathLength > bezierMaxHandleLengthCoefficient * directPathLength                
                result(Cindex) = result(Cindex) + rankWeight(10);
            end    
            if BPathLength < bezierMinHandleLengthCoefficient * directPathLength                
                result(Cindex) = result(Cindex) + rankWeight(10);
            end
            
            % ���� ���������� ����� ������������ ������� ������� ����,
            % ����������� ����� �����
            handleDistance = sqrt(sum((pointB - pointC).^2));
            if handleDistance < bezierMinHandleLengthCoefficient * directPathLength
                result(Bindex) = result(Bindex) + rankWeight(10);
                result(Cindex) = result(Cindex) + rankWeight(10);
            end
        end
    end
end

% ��������� ������ ����� �� ��������� ���������
function result = getPointRank(indexPoint, lengthPointWeight, linkPoint, bezierSupplyRank, rankThreshold)
    result = zeros(numel(indexPoint)/2, 10);
    
    angleThreshold = rankThreshold(1); % ����� ����������� ������������/����������������/��������������/������������ ��������
    lengthHandleThreshold = rankThreshold(2); % ����� ��� ����������� �������������� �������
    lengthPathThreshold = rankThreshold(3); % ����� ��� ����������� ��������� �������
    
    for i = 1:numel(indexPoint)/2

        siblingPointsCount = result(i, 1); 
        horLinesCount = result(i, 2); 
        vertLinesCount = result(i, 3); 
        is90Divisible = result(i, 4); 
        isHandlesEqual = result(i, 5);
        sibling90Penalty = result(i, 6);
        siblingHandlesEqualPenalty = result(i, 7);
        shortPathPenalty = result(i, 8);
        fakePointPenalty = result(i, 9);
        
        if (linkPoint(i, 1) ~= 0)
            % ���� �������� �����
            siblingPointsCount = 1;
            
            % ��������� ���� ������� �������
            d1 = indexPoint(linkPoint(i, 1), 1:2) - indexPoint(i, 1:2);
            a1 = 90;
            if d1(1) ~= 0
                a1 = atan(d1(2)/d1(1)) * 180 / pi;
            else
                a1 = d1(2)/abs(d1(2)) * a1;
            end
            
            % �� ���� ������� ����, �������������� ������� ��� ������������
            if abs(a1) < angleThreshold
                horLinesCount = horLinesCount + 1;
            end
            if (abs(a1) > angleThreshold) && (mod(a1, 90) < angleThreshold)
                vertLinesCount = vertLinesCount + 1;
            end
            
            % ���� ������� ��������, ������ �����
            if (lengthPointWeight(i, 1)) < lengthPathThreshold
                shortPathPenalty = 1;
            end
        end
        if (linkPoint(i, 2) ~= 0)
            % ���� �������
            siblingPointsCount = 2;
            
            % ��������� ���� ������� �������
            d2 = indexPoint(linkPoint(i, 2), 1:2) - indexPoint(i, 1:2);
            a2 = 90;
            if d2(1) ~= 0
                a2 = atan(d2(2)/d2(1)) * 180 / pi;
            else
                a2 = d2(2)/abs(d2(2)) * a2;
            end
            
            % �� ���� ������� ����, �������������� ������� ��� ������������
            if abs(a2) < angleThreshold
                horLinesCount = horLinesCount + 1;
            end
            if (abs(a2) > angleThreshold) && (mod(a2, 90) < angleThreshold)
                vertLinesCount = vertLinesCount + 1;
            end     
            
            % ���� ����� - ������� ���� ��� �����, ����� ���������,
            % �������� �� �������� ������������ ����������� ���
            % ��������������� (��� ����������� �� ������������/�������������� ����������)
            if ((linkPoint(i, 3) == 0) || (linkPoint(i, 3) == 1)) && ((mod(a2 - a1, 90) < angleThreshold) || (abs(a2 - a1) < angleThreshold))
                is90Divisible = 1;
               
                % ������� ����� �������� �����, ������ ��� ����� �������
                % ������� ������� ��������� ��������� �� ���������
                result(linkPoint(i, 1), 6) = result(linkPoint(i, 1), 6) + 1;
                result(linkPoint(i, 2), 6) = result(linkPoint(i, 2), 6) + 1;
            end
            
            % ���� ����� �������, � ������� ����� - ����, �����
            % ������������ ���� �� ��������� �� �������� ����������������
            % �� �����. ��� �����������, ������ ��� ��� ��������� �����
            % ������������� ��� ������������ �����
            if (linkPoint(i, 3) == 0) && (linkPoint(linkPoint(i, 1), 3) == 1) && (linkPoint(linkPoint(i, 2), 3) == 1)
                if ((sqrt(d1(1)*d1(1) + d1(2)*d1(2)) - sqrt(d2(1)*d2(1) + d2(2)*d2(2))) < lengthHandleThreshold)
                    isHandlesEqual = 1;
                    
                    % �������������� ������� ����� �������� �����, ������ ��� ����� �������
                    % ������� ������� ��������� ��������� �� ���������
                    result(linkPoint(i, 1), 7) = result(linkPoint(i, 1), 7) + 1;
                    result(linkPoint(i, 2), 7) = result(linkPoint(i, 2), 7) + 1;
                end
            end
            
            % ���� ����������� ����� �������� ������ ������, ������ �����
            if min(lengthPointWeight(i, :)) < lengthPathThreshold
                shortPathPenalty = 1;
            end            
        end
        
        % ���� ����� ����� ����������� ��������, ������ �����
        if linkPoint(i, 3) == 2
            fakePointPenalty = 1;
        end
              
        result(i, :) = [siblingPointsCount, horLinesCount, vertLinesCount, is90Divisible, isHandlesEqual, sibling90Penalty, siblingHandlesEqualPenalty, shortPathPenalty, fakePointPenalty, bezierSupplyRank(i)];
    end
end