function [canvas, palette] = getSymbolSquare(pathArray)
    pathCount = numel(pathArray)/8;
        
    M = [-1 3 -3 1; 3 -6 3 0; -3 3 0 0; 1 0 0 0];
    scaleK = 50;
    resolution = 5000;

    u = linspace(0,1,resolution);
    u = u';
    unit = ones(size(u));
    U = [u.^3 u.^2 u unit];
    
    resultXVector = zeros(resolution, pathCount);
    resultYVector = zeros(resolution, pathCount);

    for i = 1:pathCount
        pt1 = [pathArray(i, 1) -pathArray(i, 2)]';
        pt2 = [pathArray(i, 3) -pathArray(i, 4)]';
        pt3 = [pathArray(i, 5) -pathArray(i, 6)]';
        pt4 = [pathArray(i, 7) -pathArray(i, 8)]'; 
        
        B = [pt1 pt2 pt3 pt4]';

        A = M*B;
        Pu = U*A;
        
        resultXVector(:, i) = Pu(:,1);
        resultYVector(:, i) = Pu(:,2);
    end

    resultXVector = round(resultXVector * scaleK);
    resultYVector = round(resultYVector * scaleK);
        
    maxX = max(max(resultXVector));
    minX = min(min(resultXVector));
    maxY = max(max(resultYVector));
    minY = min(min(resultYVector));
    
    shiftX = 0 - minX + 1;
    shiftY = 0 - minY + 1;
    
    maxX = maxX + shiftX;
    maxY = maxY + shiftY;
    
    canvas = zeros(maxY, maxX);
    for i = 1:numel(resultXVector)
       canvas(1 + maxY - (resultYVector(i) + shiftY), resultXVector(i) + shiftX) = 1; 
    end
    
    [canvas, palette] = recursiveFill(canvas);
end

function [result, palette] = recursiveFill(rasterizedSymbol)
    EMPTY_COLOR = 0;
    fillStep = 0.1;
    fillColor = EMPTY_COLOR + fillStep;
    result = rasterizedSymbol;
    resultSquare = numel(result);
    squareThreshold = resultSquare * 0.005;
    palette = [];

    while (fillColor < 1)
        [startY, startX, ~] = find(result == EMPTY_COLOR);
        if (numel(startY) == 0)
            break;
        end
        
        result = fillPoint(result, fillColor, [startY(1), startX(1)]);
        [startY, ~, ~] = find(result == fillColor);
        
        % ����������� ������ ������� �� �������
        startYCount = numel(startY);
        if (startYCount < squareThreshold)
            startYCount = 0;
        end
        
        palette = [palette; startYCount];
        
        fillColor = fillColor + fillStep;
    end
end

function result = fillPoint(currentImg, currentColor, startPoint)
    EMPTY_COLOR = 0;    
    result = currentImg;
    [maxY, maxX] = size(result);
    stackLength = numel(result)*4;
    stackPointer = 1;
    lastPointer = stackPointer;
    queueStack = zeros(stackLength, 2) - 2; % ����� ������� -2 �� ������ ��������
    queueStack(stackPointer, :) = startPoint;
    
    for i = 1:stackLength
        if (queueStack(i, 1) == -2) % ���� ������� �����, �������
            break;
        end
        
        if (queueStack(i, 1) > maxY) || (queueStack(i, 2) > maxX) || (queueStack(i, 1) < 1) || (queueStack(i, 2) < 1)
           continue;
        end
       
        if result(queueStack(i, 1), queueStack(i, 2)) == EMPTY_COLOR 
            result(queueStack(i, 1), queueStack(i, 2)) = currentColor;
            
            queueStack(lastPointer+1, :) = [queueStack(i, 1), queueStack(i, 2)-1];
            queueStack(lastPointer+2, :) = [queueStack(i, 1), queueStack(i, 2)+1];
            queueStack(lastPointer+3, :) = [queueStack(i, 1)-1, queueStack(i, 2)];
            queueStack(lastPointer+4, :) = [queueStack(i, 1)+1, queueStack(i, 2)];
            
            lastPointer = lastPointer + 4;
        end
           
    end   
end