function drawSVGPath(pathArray, markArray) 
    pathCount = numel(pathArray)/8;
        
    figure;
    axis equal;

    M = [-1 3 -3 1; 3 -6 3 0; -3 3 0 0; 1 0 0 0];

    u = linspace(0,1,5000);
    u = u';
    unit = ones(size(u));
    U = [u.^3 u.^2 u unit];

    hold on
    for i = 1: pathCount
        pt1 = [pathArray(i, 1) -pathArray(i, 2)]';
        pt2 = [pathArray(i, 3) -pathArray(i, 4)]';
        pt3 = [pathArray(i, 5) -pathArray(i, 6)]';
        pt4 = [pathArray(i, 7) -pathArray(i, 8)]'; 
        
        B = [pt1 pt2 pt3 pt4]';

        A = M*B;
        Pu = U*A;

        plot(pt1(1), pt1(2), 'x');
        plot(pt2(1), pt2(2), 'o');
        plot(pt3(1), pt3(2), 'o');
        plot(pt4(1), pt4(2), 'x');
        
        plot(Pu(:,1), Pu(:,2));        
    end
    
    if exist('markArray','var')
        coord = markArray{1, 1};
        title = markArray{1, 2};
        for i = 1:numel(coord)/2            
            plot(coord(i, 1), -coord(i, 2), '+');
            text(coord(i, 1), -coord(i, 2), title(i, :), 'VerticalAlignment', 'top');
        end
    end
    
    hold off          
end