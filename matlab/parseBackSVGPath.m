function result = parseBackSVGPath(bezierMatrix)
    result = '';

    %testSVGPath = 'M 3.671875 -10.0625 C 3.671875 -10.703125 3.03125 -11.34375 2.40625 -11.34375 C 1.75 -11.34375 1.125 -10.65625 1.125 -10.0625 C 1.125 -9.453125 1.75 -8.78125 2.40625 -8.78125 C 3.03125 -8.78125 3.671875 -9.40625 3.671875 -10.0625 Z M 0.625 -7.171875 L 0.625 -6.359375 C 1.6875 -6.359375 1.671875 -6.484375 1.671875 -5.859375 L 1.671875 -0.953125 L 0.578125 -0.953125 L 0.578125 0.015625 C 1.0625 -0.03125 2.171875 -0.03125 2.59375 -0.03125 C 3.046875 -0.03125 4.0625 -0.03125 4.5625 0.015625 L 4.5625 -0.953125 L 3.5625 -0.953125 L 3.5625 -7.4375 L 0.625 -7.296875 Z M 0.625 -7.171875 ';
    %bezierMatrix = parseSVGPath(testSVGPath);
    
    lastPoint = NaN;
    
    for currentPath = 1:(numel(bezierMatrix)/8)-1
        pointA = bezierMatrix(currentPath, 1:2);
        pointB = bezierMatrix(currentPath, 3:4);
        pointC = bezierMatrix(currentPath, 5:6);
        pointD = bezierMatrix(currentPath, 7:8);
        
        if isnan(lastPoint)
            result = ['M ' num2str(pointA(1)) ' ' num2str(pointA(2)) createLine(pointB, pointC, pointD)];
        else
            if isequal(lastPoint, pointA)
                result = [result createLine(pointB, pointC, pointD)];
            else
                result = [result ' Z M ' num2str(pointA(1)) ' ' num2str(pointA(2)) createLine(pointB, pointC, pointD)];
            end
        end
        
        lastPoint = pointD;
    end
        
    pointD = bezierMatrix(numel(bezierMatrix)/8, 7:8);
    result = [result ' Z M ' num2str(pointD(1, 1)) ' ' num2str(pointD(1, 2))];
end

function result = createLine(pointB, pointC, pointD)    
    if isequal(pointB, pointC)
        result = [' L ' num2str(pointD(1)) ' ' num2str(pointD(2))];
    else
        result = [' C ' num2str(pointB(1)) ' ' num2str(pointB(2)) ' ' num2str(pointC(1)) ' ' num2str(pointC(2)) ' ' num2str(pointD(1)) ' ' num2str(pointD(2))];    
    end
end