function processSVGPath(jsonPath, outputJsonPath, countThreshold, protectedPages)
    sourcePathStruct = json.read(jsonPath);
    resultPathStruct = sourcePathStruct;
    
    lengthThreshold = 1;     
    
    if ~exist('countThreshold','var')
        countThreshold = 4; 
    end    
    
    if ~exist('protectedPages','var')
        protectedPages = 1:sourcePathStruct.pageCount;
    end      
    
    tic;
    
    % ������������� ������� �� �������� ����������
    [processedPathStruct, protectedPagesStat] = filterSVGPath(sourcePathStruct, countThreshold, lengthThreshold, protectedPages);
    embeddableSymbolsCount = 0;
    
    MINIMAL_SUMMARY_RANK = 0.003;
    
    candidateToReplaceSymbolsCount = numel(protectedPagesStat)/(numel(protectedPages)+2)
    
    filterForThisId = {
    
    };
% 'svg_6_75'
    for i = 1:candidateToReplaceSymbolsCount
        newPathName = strcat(protectedPagesStat{i}, '_m');
        originalSymbol = processedPathStruct.symbols.(protectedPagesStat{i, 1});
        
        % ��� ���������� �� ��������� ������ ����� ��������� filterForThisId
        f = [];
        for j = 1:numel(filterForThisId)
           if logical(strcmp(protectedPagesStat{i, 1}, filterForThisId{j}))
               f = find(processedPathStruct.symbols.(protectedPagesStat{i, 1}).resultRankPoint == 1); % ~= Inf
               break;
           end
        end
        
        % ���������������� ��� ������ �� ��������� ������
        f = find(originalSymbol.resultRankPoint == 1); % ~= Inf
        
        if ~logical(numel(f))
            % resultPathStruct.symbols.(newPathName) = struct('path', parseBackSVGPath(originalSymbol.bezier));       
            % resultPathStruct.symbols.(protectedPagesStat{i}).mod = newPathName;        
            continue;
        end
        
        % Progress count
        ['��������� ', num2str(i), ' �� ', num2str(candidateToReplaceSymbolsCount)]
        
        % coolPoints = { originalSymbol.indexPoint(f, :), num2str(processedPathStruct.symbols.(protectedPagesStat{i, 1}).rankPoint(f, :))};
        
        changedSymbol = originalSymbol;
        changedFlag = false;
        
        for currentCandidatePointIndex = 1:numel(f)
            rank = rankPathChange(processedPathStruct.symbols.(protectedPagesStat{i, 1}), f(currentCandidatePointIndex));

            if (rank(3) > 0)
               changedFlag = true;                 
               changedSymbol.bezier = pathChange(changedSymbol, f(currentCandidatePointIndex), rank(1:2));  
            end
        end 
        
        % ����������� ��� ���������. �������, ��� ��������� ���������
        % ������ ������� �� �����������. �� ����� ���� ��� �� ������ ���,
        % �� ������� ��� ���������� ������, � ���������� ������ ���������
        % �� ����� ����� �������, ����������
        if (changedFlag)               
            [canvas1, palette1] = getSymbolSquare(processedPathStruct.symbols.(protectedPagesStat{i, 1}).bezier);
            [~, palette2] = getSymbolSquare(changedSymbol.bezier);

            summaryRank = getSymbolSquareDiff(palette1, palette2) / numel(canvas1);
            
            if summaryRank > MINIMAL_SUMMARY_RANK
                %drawSVGPath(processedPathStruct.symbols.(protectedPagesStat{i, 1}).bezier, coolPoints);
                %title(protectedPagesStat{i, 1}); 

                %drawSVGPath(changedSymbol.bezier, coolPoints);
                %title([protectedPagesStat{i, 1}, ' changed ', num2str(summaryRank)]);
                
                %protectedPagesStat{i, 1}
                %summaryRank

                %figure; imshow(canvas1);
                %figure; imshow(canvas2);
                
                embeddableSymbolsCount = embeddableSymbolsCount + 1;
                ['����� ������ ��� ���������: ', (protectedPagesStat{i}), '. ����� ', num2str(embeddableSymbolsCount)]
                
               % ������������� ����� �������
                resultPathStruct.symbols.(newPathName) = struct('path', parseBackSVGPath(changedSymbol.bezier));       
                resultPathStruct.symbols.(protectedPagesStat{i}).mod = newPathName;                
            end
        end 
    end
    
    embeddableSymbolsCount
    
    
    % ������� ������� ������ �������� � �����
    % protectedPagesCount = numel(protectedPages);
    % for i = 1:protectedPagesCount
    %     sum(cell2mat(protectedPagesStat(:, i+2)))
    % end
    
    resultPathStruct.countThreshold = countThreshold;
    resultPathStruct.protectedPages = protectedPages;
    
    json.write(resultPathStruct, outputJsonPath, 'Indent', 4);
    
    toc
end