% ��������� ������������� ���������� ��������� �������:
% 1. RGB-������������� ������� � ������������ ����������� ��������� �����
% �����������
% 2. RGB-������������� ������������ ����������, � ����� ��� ���������������
% ���� � ������� ����� ���������� �� ���� � ���������� �������������
% ����������� ��������������� ������������ ���������� �� ������ � ����
% 3. ����������� ��������� ����� ���������� � �������� ������ ��� ���������
% ������� � ����������� ����������� � ��������������� ����������� ������
% ���������� ��������� � ���������� ���������� ������� ����������
%
% � ������ ���������� ���� ���� ������� ����������� ��������� �����������
% �� ���� � �������� ������� � ���������� �������.
%
% ������ �� ���������� ������ ������������, ��� ��� ����������
% RGB-������������� � ������� � ������������ ����������� �������� ������
% ��������������, �.�. ����������� ������������ ���������� � ������������
% RGB � ����������� ������������ ������� � ��������������� ����������� ����
% �������� � ��������

function result = testPaletteEmbedding(sourceFileName, targetFileName)
    % ������������������� �����
    result = false;
    indexEqual = false;
    RGBEqual = false;

    % ����� ��������������� ��������, ������� � RGB-������� �� ������
    % ��������
    [C_IPi, C_IPm, C_RGB] = getImages(sourceFileName);
    
    % �������� ������ ���������� ��������������� �������� � �������
    [S_IPi, S_IPm] = makePaletteEmbedding(C_IPi, C_IPm);
    
    % ������� RGB-������� ������������ ����������
    S_RGB = ind2rgb(S_IPi, S_IPm);
    
    % �������� ����. ������, ���� RGB-������� ���������,
    if (isequal(C_RGB, uint8(S_RGB*255)) || isequal(C_RGB, S_RGB))
        % ����� ����� ����� ��������� ����������� ��������� � ����,
        % � ����� ���������, ��� � ����� ������ �� ��������.
        imwrite(S_IPi, S_IPm, targetFileName);
        [test_IPi, test_IPm, test_RGB] = getImages(targetFileName);
        
        if (isequal(S_RGB, test_RGB) && isequal(S_IPi, test_IPi) && isequal(S_IPm, test_IPm)) 
            indexEqual = true
        end
        
        % ������ ��������, ��� �������������� � ������������ RGB � ������� 
        % ���� ��� �������� ������ ���������
        imwrite(S_RGB, targetFileName);
        [test_IPi, test_IPm, test_RGB] = getImages(targetFileName);
        
        if (isequal(test_IPi, C_IPi) && isequal(test_IPm, C_IPm))
            RGBEqual = true
        end
    end
   
    % ������� ��������� �����
    delete(targetFileName);
    
    % ��� ���������� ���� ������� �������, ��� ���� �������
    % � �������� ����������� ���������
    if (indexEqual && RGBEqual)
        imwrite(S_IPi, S_IPm, targetFileName);
        result = true;
    end
end

function [indexedImage, map, fullRGB] = getImages(sourceFileName)
    [A, B] = imread(sourceFileName); 

    if isempty(B)
        % Full RGB
        fullRGB = A;
        [indexedImage, map] = rgb2ind(fullRGB, 256); 
    else   
        % ��������������� �����������
        indexedImage = A;
        map = B;
        fullRGB = ind2rgb(indexedImage, map);
    end
end