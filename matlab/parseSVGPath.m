function result = parseSVGPath(path)    
    result = zeros(0, 8);
    operationStack = 0;
    lastPoint = struct('x', NaN, 'y', NaN);
    lastOperationPoint = 0;
    lastMPoint = 0;
    tokens = strsplit(path, ' ');
    
    for i = 1:numel(tokens)
        currentToken = tokens(i);   
        currentToken = currentToken{1};
        
        switch currentToken
            case 'M'
                operationStack = 0;
                lastPoint = struct('x', NaN, 'y', NaN);
                lastOperationPoint = 0;
                lastMPoint = 0;
                
                [operationStack, result, lastMPoint] = operationStackInit(operationStack, result, lastMPoint);
                operationStack.operation = 'M';
            case 'L'
                [operationStack, result, lastMPoint] = operationStackInit(operationStack, result, lastMPoint);
                operationStack.operation = 'L';
                operationStack.coords = [operationStack.coords, lastOperationPoint.x, lastOperationPoint.y];                 
            case 'C'
                [operationStack, result, lastMPoint] = operationStackInit(operationStack, result, lastMPoint);
                operationStack.operation = 'C';
                operationStack.coords = [operationStack.coords, lastOperationPoint.x, lastOperationPoint.y];   
            case 'Z'
                [operationStack, result, lastMPoint] = operationStackInit(operationStack, result, lastMPoint);
                operationStack.operation = 'Z';          
                operationStack.coords = [operationStack.coords, lastOperationPoint.x, lastOperationPoint.y];
                [operationStack, result, lastMPoint] = operationStackInit(operationStack, result, lastMPoint);                
            otherwise
                lastPoint = setLastCoord(lastPoint, str2double(currentToken));
                tempCoord = getLastCoord(lastPoint);
                
                if isstruct(tempCoord)
                    operationStack.coords = [operationStack.coords, tempCoord.x, tempCoord.y];
                    lastOperationPoint = lastPoint;
                    lastPoint = struct('x', NaN, 'y', NaN);
                end
        end      
    end
end

function [newOpStack, newResult, lastMPoint] = operationStackInit(operationStack, operationsTree, lastMPoint)
        
    newOpStack = struct('operation', '', 'coords', []);
    newResult = operationsTree;    

    if isstruct(operationStack)
        if operationStack.operation == 'M'
            lastMPoint = operationStack.coords;
        elseif operationStack.operation == 'C'
            newResult = [newResult; operationStack.coords];
        elseif operationStack.operation == 'L'
            meanX = mean([operationStack.coords(1), operationStack.coords(3)]);
            meanY = mean([operationStack.coords(2), operationStack.coords(4)]);
            newResult = [newResult; [operationStack.coords(1), operationStack.coords(2), meanX, meanY, meanX, meanY, operationStack.coords(3), operationStack.coords(4)]];    
        elseif operationStack.operation == 'Z'
            
            meanX = mean([operationStack.coords(1), lastMPoint(1)]);
            meanY = mean([operationStack.coords(2), lastMPoint(2)]);
            
            newResult = [newResult; [operationStack.coords(1), operationStack.coords(2), meanX, meanY, meanX, meanY, lastMPoint(1), lastMPoint(2)]];            
        end
    end    
end

function result = setLastCoord(lastPoint, value)

    if (isnan(lastPoint.x) && isnan(lastPoint.y)) || (not(isnan(lastPoint.y)))
        result = struct('x', value, 'y', NaN);
    else
        result = struct('x', lastPoint.x, 'y', value);
    end
end

function result = getLastCoord(lastPoint)
    if isnan(lastPoint.y)
        result = 0;
    else
        result = lastPoint;
    end
end

