% ��������� ����������� � ������� ���������������� �����������, �����
% ������� ������ � �������� ������� �������

function [resultIndexedImage, resultMap] = makePaletteEmbedding(sourceIndexedImage, sourceMap)
    % ����������������� ������ ����������
    resultMap = sourceMap;
    resultIndexedImage = sourceIndexedImage;

    % ������ � �������, ������ � �������
    for i = 1:(numel(sourceMap)/3)      
        if (logical(mod(i, 2)))           
            resultMap(i+1, :) = sourceMap(i, :); % 1 -> 2       
        else
            resultMap(i-1, :) = sourceMap(i, :); % 2 -> 1        
        end
    end
    
    % ������ �� ������, ������ � ����
    for i = 0:((numel(sourceMap)/3)-1) 
        if (logical(mod(i, 2)))
            [oldRow, oldCol] = find(sourceIndexedImage == i);
            for j = 1:numel(oldRow)
                resultIndexedImage(oldRow(j), oldCol(j)) = i-1;    
            end          
        else
            [oldRow, oldCol] = find(sourceIndexedImage == i);
            for j = 1:numel(oldRow)
                resultIndexedImage(oldRow(j), oldCol(j)) = i+1;    
            end          
        end        
    end
end
